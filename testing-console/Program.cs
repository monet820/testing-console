﻿using System;
using System.Linq;

namespace testing_console
{
    class Program
    {
        static void Main(string[] args)
        {
            int usernameLength = 0;
            string firstLetter;
            string username;
            Console.WriteLine("Hello");
            Console.WriteLine("My name is Henrik");
            Console.WriteLine("Please type your name.");
            username = Console.ReadLine();
            firstLetter = username.Substring(0,1);
            usernameLength = username.Length;
            Console.WriteLine("Hello " + username + " your name is: " + usernameLength + " characters long, and starts with an " + firstLetter);
        }
    }
}
